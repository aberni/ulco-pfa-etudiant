-- safeTailString
safeTailString :: String -> String
safeTailString ""     = ""
safeTailString (x:xs) = xs

-- safeHeadString 
safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (x:xs) = Just x

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (x:xs) = xs

-- safeHead
safeHead :: [a] -> Maybe a
safeHead [] = Nothing 
safeHead (x:xs) = Just x

main :: IO ()
main = do
    print $ safeTailString "foobar"

