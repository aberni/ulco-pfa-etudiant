import Options.Applicative

data Args = Args
    { outfile    :: String
    , hello      :: String
    , val1       :: Int
    , val2       :: Double
    } deriving (Show)

argsP :: Parser Args
argsP = Args
      <$> strArgument (metavar "<fichier de sortie>")
      <*> strOption
          ( long "hello"
         <> help "Target for the greeting"
         <> metavar "TARGET")
      <*> option auto
          ( long "val1"
         <> help "Value 1"
         <> metavar "INT"
         <> value 1)
      <*> option auto
          ( long "val2"
         <> help "Value 2"
         <> metavar "DOUBLE"
         <> value 1)

fileP :: Parser String
fileP = strArgument (metavar "<fichier d'entree'>")

fullP :: Parser String -> Parser Args
fullP =  fileP <*> argsP

myinfo :: ParserInfo Args
myinfo = info (fullP <**> helper)
              (fullDesc <> header "This is my cool app!")

main :: IO ()
main = do
    args <- execParser myinfo
    print args

