{-# LANGUAGE OverloadedStrings #-}

import Data.Text

newtype Person = Person Text deriving Show

persons :: [Person]
persons = [Person "John", Person "Haskell"]

main :: IO ()
main = print persons

{-
ou alors

import Data.Text as T

newtype Person = Person Text deriving Show

persons :: [Person]
persons = [Person (T.pack "John"), Person (T.pack "Haskell")]

main :: IO ()
main = print persons

 -}

