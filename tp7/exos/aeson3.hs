{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    res1 <- eitherDecodeFileStrict "aeson-test1.json"
    print (res1 :: Either String Person)

