-- seuilInt
seuilInt :: Int -> Int -> Int -> Int
seuilInt min max v
    | v < min = min
    | v > max = max
    | otherwise = v


-- seuilTuple
seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (x0, x1) x = max x0 $ min x1 x

main :: IO ()
main = do
    print $ seuilInt 1 10 0
    print $ seuilInt 1 10 2
    print $ seuilInt 1 10 42
    print $ seuilTuple (1, 10) 0
    print $ seuilTuple (1, 10) 2
    print $ seuilTuple (1, 10) 42

