import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

saisirEntier :: Int -> IO ()
saisirEntier i = do
    putStr $ "saisie " ++ show i ++ " : "
    hFlush stdout
    res <- getLine
    case readMaybe res :: Maybe Int of
        Just x  -> putStrLn $ "-> vous avez saisi l'entier " ++ res
        Nothing -> putStrLn "-> saisie invalide"

main :: IO ()
main = forM_ [1..4] saisirEntier


