
data List a = Nil | Cons a (List a)
    deriving Show

sumList :: List a -> a
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs


-- concatList 
concatList :: List a -> List a -> List a
concatList Nil ys = ys
concatList (Cons x xs) ys = Cons x (concatList xs ys)

-- flatList

-- toHaskell
toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons h t) = h:toHaskell t

-- fromHaskell 
fromHaskell :: [a] -> List a
fromHaskell [] = Nil
--fromHaskell (x:xs) = 

-- myShowList
myShowList :: Show a => List a -> String
myShowList Nil = ""
myShowList (Cons x xs) = show x ++ " " ++ myShowList xs

main :: IO ()
main = do
    print $ "TODO"

