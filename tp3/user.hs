
data User = User
    {
        _nom    :: String,
        _prenom :: String,
        _age    :: Int
    } 
-- deriving Show

showUser :: User -> String
showUser u = "nom: " ++ _nom u ++ "; prenom: " ++ _prenom u ++ "; age: " ++ show (_age u)

incAge :: User -> User
incAge u = u { _age = _age u + 1 }

main :: IO ()
main = do
    let antoine = User "Bernier" "Antoine" 27
    print $ showUser antoine
    let antoineVieux = incAge antoine
    print $ showUser antoineVieux