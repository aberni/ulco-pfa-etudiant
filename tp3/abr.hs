import Data.List (foldl')

data Abr = Empty | Node Int Abr Abr
    deriving (Show)

-- insererAbr 
insererAbr :: Int -> Abr -> Abr
insererAbr x Empty = Node x Empty Empty
insererAbr x (Node y l r)   | x < y     = Node y (insererAbr x l) r
                            | otherwise = Node y l (insererAbr x r)

-- listToAbr 
listToAbr :: [Int] -> Abr 
listToAbr [] = Empty
listToAbr (x:xs) = insererAbr x (listToAbr xs)

-- abrToList 
abrToList :: Abr -> [Int]
abrToList Empty = []
abrToList (Node x l r) = abrToList l ++ [x] ++ abrToList r

main :: IO ()
main = do
    print $ listToAbr [42, 13]
    print $ abrToList $ listToAbr [42, 13, 37]

