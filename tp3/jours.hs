
data Jour = Lendi | Môrdi | Credi | Joudi | Dredi | Sadi | Gromanche
    deriving Show

estWeekend :: Jour -> Bool
estWeekend Sadi = True
estWeekend Gromanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables = length . filter (not . estWeekend)

main :: IO ()
main = do
    print $ estWeekend Gromanche
    print $ estWeekend Môrdi
    print $ compterOuvrables [Lendi, Môrdi, Credi, Joudi, Dredi, Sadi, Gromanche]


