{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Text
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Artist = Artist
    {   _id :: Int
    ,   _name  :: Text
    } deriving (Generic, Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field 

selectAllArtists :: Connection -> IO [Artist]
selectAllArtists conn = query_ conn "SELECT * \
                \FROM artist"
                

main :: IO ()
main = withConnection "music.db" selectAllArtists >>= mapM_ print