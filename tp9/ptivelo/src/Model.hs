{-# LANGUAGE DeriveGeneric #-}

module Model where

import Data.Aeson
import GHC.Generics
import qualified Data.Text.Lazy as L

data Rider = Rider
    { _name :: L.Text
    , _imgs :: [L.Text]
    } deriving Generic

instance ToJSON Rider