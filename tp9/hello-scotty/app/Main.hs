{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson hiding (json)
import GHC.Generics
import qualified Data.Text.Lazy as L

import Lucid

import Web.Scotty
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)

import TextShow


data Person = Person { _name::String, _age::Int } deriving Generic

instance ToJSON Person

myPage :: Html ()
myPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "Mon tp9"
        body_ $ do
             h1_ "this is"
             ul_ $ mapM_ mkLi
                    [  "route1"
                    , "route1/route2"
                    , "html1"
                    , "json1"
                    , "json2"
                    , "add1/15/30"
                    , "add2?x=10&y=20"
                    , "index"
                    ]
             img_ [src_ "bob.png"]
            
mkLi :: L.Text -> Html ()
mkLi txt = li_ $ a_ [href_ (L.toStrict txt)] $ toHtml txt

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware $ staticPolicy $ addBase "static"

    get "/" $ text "this is /"
    get "/route1" $ text "this is route1"
    get "/route1/route2" $ text "/page1/subpage2"
    get "/html1" $ html "<h1>This is html1</h1>"
    get "/json1" $ json $ L.pack "this json1"
    get "/json2" $ json (Person "toto" 42)
    get "/lucid" $ html $ renderText myPage
    get "/add1/:x/:y" $ do
        x <- param "x"
        y <- param "y"
        text $ showtl (x+y :: Int)
    get "/add2" $ do
        x <- param "x" `rescue` (\_ -> return 0)
        y <- param "y" `rescue` (\_ -> return 0)
        text $ showtl (x+y :: Int)
    get "/index" $ redirect "/"
